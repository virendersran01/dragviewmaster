package com.example.dragviewexample;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getSimpleName();
    private TextView dragMe;
    float xDown = 0, yDown = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dragMe = findViewById(R.id.dragMe);
        dragMe.setOnTouchListener(new OnTouchEvent());
    }

    private class OnTouchEvent implements View.OnTouchListener {

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    Log.d(TAG, "onTouch: MotionEvent.ACTION_DOWN");
                    Log.d(TAG, "onTouch: ACTION_DOWN X "
                            + dragMe.getX());
                    Log.d(TAG, "onTouch: ACTION_DOWN Y "
                            + dragMe.getY());
                    xDown = motionEvent.getX();
                    yDown = motionEvent.getY();

                    break;
                case MotionEvent.ACTION_UP:
                    Log.d(TAG, "onTouch: MotionEvent.ACTION_UP");
                    Log.d(TAG, "onTouch: ACTION_UP X "
                            + dragMe.getX());
                    Log.d(TAG, "onTouch: ACTION_UP Y "
                            + dragMe.getY());
                    Toast.makeText(MainActivity.this,
                            "View X: " + dragMe.getX() +
                                    " View Y: " + dragMe.getY(), Toast.LENGTH_LONG).show();
                    break;
                case MotionEvent.ACTION_MOVE:
                    Log.d(TAG, "onTouch: MotionEvent.ACTION_MOVE");
                    float xMove, yMove;
                    xMove = motionEvent.getX();
                    yMove = motionEvent.getY();

                    float distX = xMove - xDown;
                    float distY = yMove - yDown;
                    Log.d(TAG, "onTouch: ACTION_MOVE distance X "
                            + dragMe.getX());
                    Log.d(TAG, "onTouch: ACTION_MOVE distance Y "
                            + dragMe.getY());

                    dragMe.setX(dragMe.getX() + distX);
                    dragMe.setY(dragMe.getY() + distY);
                    Log.d(TAG, "onTouch: ACTION_MOVE X "
                            + dragMe.getX());
                    Log.d(TAG, "onTouch: ACTION_MOVE Y "
                            + dragMe.getY());
                    break;
            }
            return true;
        }
    }
}